<?
/******************************
Live Composer Plugin Functions
*******************************/

function riz3_lc_defaults( $options, $id ) {

	//colors and type variables based on a light theme
	$font1 = 'Roboto Condensed'; //running copy, bold titles, subtitles
	$font2 = 'Maven Pro'; //titles, subtitles

	$color1 = '#439be9'; //blue (buttons, links, some titles)
	$color1_hover = '#2f87d4'; //darker blue (hover state)
	$color2 = '#494949'; //gray (running copy, some titles, subtitles)
	$color3 = '#e8c413'; //yellow


	// The array that will hold new global defaults
	$new_global_defaults = array(
		'css_title_font_weight' => '300',
		'css_title_font_family' => $font1,

		'css_main_font_family' => $font1,
		'css_main_font_weight' =>'300',
		'css_main_font_size' => '18',
		'css_main_color' => $color2,
		'css_main_line_height' => '27',

		'css_h1_font_family' => $font1,
		'css_h1_font_size' => '70',
		'css_h1_color' => $color2,
		'css_h1_font_weight' =>'700',
		'css_h1_line_height' => '60',

		'css_h2_font_family' => $font2,
		'css_h2_font_weight' =>'700',
		'css_h2_font_size' => '40',
		'css_h2_line_height' => '45',
		'css_h2_color' => '#707070',

		'css_h3_font_size' => '30',
		'css_h3_color' => $color1,
		'css_h3_font_weight' =>'300',
		'css_h3_font_family' => $font1,
		'css_h3_line_height' => '30',

		'css_h4_font_size' => '30',
		'css_h4_color' => $color2,
		'css_h4_font_weight' =>'400',
		'css_h4_font_family' => $font2,
		'css_h4_line_height' => '30',
	);

	$new_defaults = array(
	);

	// Alter defaults for text module
	if ( $id == 'DSLC_Text_Simple' ) {
		$new_defaults = array(
		);
	}

	// Alter defaults for text module
	if ( $id == 'DSLC_Posts' ) {
		$new_defaults = array(
			'title_color' => $color1,
			'title_font_size' => '30',
			'css_title_font_weight' => '300',
			'title_line_height' => '30',
			'css_title_text_transform' => 'Uppercase',
			'css_excerpt_font_size' => '14',
			'css_excerpt_line_height' => '21',
			'css_thumb_border_radius_top' => '0',
			'css_main_border_radius_bottom' => '0',
			'css_button_border_radius' => '0'
		);
	}

	if ( $id == 'DSLC_Button' ) {
		$new_defaults = array(
			'button_text' => 'Get Started',
			'css_align' => 'center',
			'css_border_radius' => '0',
			'css_margin_top' => '30',
			'css_padding_vertical' => '15',
			'css_padding_horizontal' => '40',
			'css_button_font_size' => '21',
			'css_button_font_weight' => '300',
			'css_bg_color' => $color1,
			'css_bg_color_hover' => $color1_hover,
			'button_state' => 'disabled',
		);
	}

	if ( $id == 'DSLC_Icon' ) {
		$new_defaults = array(
			'icon_id' => 'gears',
			'css_text_align' => 'center',
			'css_margin_bottom' => '30',
			'css_icon_color' => $color1,
			'css_icon_color_hover' => $color1,
			'css_icon_size' => '60',
		);
	}

	$module_defaults = array_merge($new_global_defaults, $new_defaults);
	// Call the function that alters the defaults and return
	return dslc_set_defaults( $module_defaults, $options );

} add_filter( 'dslc_module_options', 'riz3_lc_defaults', 10, 2 );
?>
