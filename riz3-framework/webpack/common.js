const path              = require('path');
const webpack           = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = () => {
  return {
    entry: {
      './riz3-framework/dist/js/scripts.js': './lib/js/scripts.js',
      './riz3-framework/dist/css/style.css': './lib/scss/style.scss',
      './riz3-child/dist/js/scripts.js': '../riz3-child/lib/js/scripts.js',
      './riz3-child/dist/css/style.css': '../riz3-child/lib/scss/style.scss',
    },
    output: {
      path: path.resolve(__dirname, '../../'),
      filename: '[name]'
    },
    resolve: {
      extensions: ['.js']
    },
    module: {
      rules: [
        {
          test: /\.js?$/,
          exclude: /node_modules/,
          use: 'babel-loader'
        },
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: ['css-loader', 'sass-loader']
          })
        }
      ]
    },
    plugins: [
      new ExtractTextPlugin({
        filename: '[name]'
      }),
      new BrowserSyncPlugin({
        host: 'localhost',
        port: 4200, //Change this to an open/different port for each project
        proxy: 'http://power-digital-test', //change to your vhost name
        reload: false,
        files: ["**/*.php","dist/css/*.css", "dist/js/*.js", "../riz3-child/**/*.php","../riz3-child/dist/css/*.css", "../riz3-child/dist/js/*.js"]
      })
    ]
  };
};
