<?php
/**
 * The profile template file
 *
*/

get_header(); ?>

<div id="page--<?php echo $post->ID; ?>" class="page page--athlete">
  <?php
    get_template_part( 'template-parts/content', 'profile' );
  ?>
</div><!-- .page -->

<?php get_footer(); ?>
