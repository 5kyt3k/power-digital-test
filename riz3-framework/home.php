<?php

/**
 *
 * @package
 * @subpackage
 * @since
 */

get_header();
$home_title = get_the_title( get_option('page_for_posts') );
?>

<div id="page--<?php echo $post->ID; ?>" class="page page--blog">

  <div class="masthead masthead--blog">
    <div class="wrap">
      <h3 class="masthead--title"><?php echo $home_title ?></h3>
    </div><!-- .wrap -->
  </div><!-- .masthead -->

  <div class="box">
    <div class="wrap">
      <div class="row">

          <div class="col-sm-8">
            <?php
            global $wp_query;
            $big = 999999999; // need an unlikely integer

            if (have_posts()) : while (have_posts()) : the_post();
              get_template_part( 'template-parts/content', 'post' );
            endwhile;?>

            <div class="pagination">
              <?php echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $wp_query->max_num_pages,
                'prev_text' => __('Previous'), //icon set with css (default fontawesome)
                'next_text' => __('Next'), //icon set with css (default fontawesome)
              ) ); ?>
            </div><!-- .pagination -->

            <?php else :
              get_template_part( 'template-parts/content', 'none' );
            endif; ?>

          </div><!-- .col-sm-8 -->

          <div class="col-sm-4">
            <?php get_sidebar(); ?>
          </div><!-- .col-sm-4 -->

      </div><!-- .row-->
    </div><!-- .wrap -->
  </div><!-- .box -->
</div><!-- .page -->

<?php get_footer(); ?>
