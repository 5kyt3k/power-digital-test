/*
Scripts File

Developed by:
URL:

Include custom js functions here. This file loads in the footer to help with load time.
*/

// as the page loads, call these scripts
jQuery(document).ready(function($) {
  //Expand Event
  $('.expand-btn').click(function() {
    $(this)
      .parents('.card-event')
      .find('.event-content')
      .slideToggle();
  });
}); /* end of as page load scripts */
