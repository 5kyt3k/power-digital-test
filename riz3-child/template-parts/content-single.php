<?php
/**
 * Template part for displaying single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package
 */
?>

<article id="post--<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry--header">
		<?php
		if ( has_post_thumbnail() ) {
			the_post_thumbnail('large');
		} else { ?>
			<img src="<?php echo get_template_directory_uri(); ?>/lib/images/default-image.jpg" />
		<?php } ?>
		<h1 class="entry--title"><?php the_title(); ?></h1>
	</header>

	<section class="entry--content">
    <?php the_content(); ?>
	</section>

	<footer class="entry--footer">
	</footer>
</article>
