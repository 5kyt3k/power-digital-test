<?php
/**
 * The template for displaying single posts
 *
 *
 * @package
 * @subpackage
 * @since
 */

 get_header(); ?>

<div id="page--<?php echo $post->ID; ?>" class="page page--single">
  <div class="wrap">
    <div class="row">
      <div class="col-sm-8">

        <?php
          if (have_posts()) : while (have_posts()) : the_post();

            //Get the post content
            get_template_part( 'template-parts/content', 'single' );

          endwhile; else :
            get_template_part( 'template-parts/content', 'none' );
          endif;
        ?>

        <div class="below-content-container">
          <div class="pagination single-pagination clearfix">
            <div class="prev-post">
                <?php
                if( get_adjacent_post(false, '', true) ) {
                    previous_post_link('%link', '<span>Previous</span>');
                } else {
                    $first = new WP_Query('posts_per_page=1&order=DESC'); $first->the_post();
                        echo '<a href="' . get_permalink() . '"><span>Previous</span></a>';
                    wp_reset_query();
                };?>
            </div><!-- .prev-post -->
            <div class="next-post">
                <?php
                if( get_adjacent_post(false, '', false) ) {
                	next_post_link('%link', '<span>Next</span>');
                } else {
                	$last = new WP_Query('posts_per_page=1&order=ASC'); $last->the_post();
                    	echo '<a href="' . get_permalink() . '"><span>Next</i></span></a>';
                    wp_reset_query();
                };?>
            </div><!-- .next-post -->
          </div><!-- .pagination -->

          <?php comments_template(); // uncomment if you want to use them ?>
        </div><!-- .below-content-container-->
      </div><!-- .col-sm-8 -->

      <div class="col-sm-4">
        <?php get_sidebar(); ?>
      </div><!-- .col-sm-4 -->

    </div><!-- .row -->
  </div><!-- .wrap -->
</div><!-- .page -->

<?php get_footer(); ?>
