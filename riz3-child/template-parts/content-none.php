<?php
/**
 * Template part for no results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package
 */
?>

<article id="post-not-found" class="hentry clearfix">
  <header class="article-header">
    <h1 class="title--no-results"><?php _e( 'Hmmm, what are you looking for?', 'riz3theme' ); ?></h1>
  </header>
  <section class="entry-content">
    <p><?php _e( 'Nothing was found based on your search.', 'riz3theme' ); ?></p>
  </section>
    <footer class="article-footer">
      <p><?php _e( 'If you believe you found this message in error please contact us.', 'riz3theme' ); ?></p>
  </footer>
</article>
