<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package
 */
?>

<article id="post--<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry--header">
		<div class="wrap">
			<h1 class="entry--title"><?php the_title(); ?></h1>
		</div><!-- .wrap -->
	</header>

	<section class="entry--content">
    <div class="wrap-sm">
      <?php the_content(); ?>
    </div><!-- .wrap-sm -->
	</section>

	<footer class="entry--footer">
	</footer>
</article>

<div class="wrap">
	<div class="row">
		<div class="col-xs-6 col-md-4">
			<div class="box box--grey">
				<h2>Col 1</h2>
			</div><!-- .box -->
		</div><!-- .col-md-4 -->
		<div class="col-xs-6 col-md-4">
			<div class="box box--grey">
				Sample Content...Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
			</div><!-- .box -->
		</div><!-- .col-md-4 -->
		<div class="col-xs-12 col-md-4">
			<div class="box box--grey">
				Col 3
			</div><!-- .box -->
		</div><!-- .col-md-4 -->
	</div><!-- .row -->
</div><!-- .wrap -->
