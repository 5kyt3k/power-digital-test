<?php
/* Welcome
This is the core-functions file where most of the
main functions & features reside.

Developed by:
URL:

------------------------------------------------------------------
- TABLE OF CONTENTS
------------------------------------------------------------------

Menus
    a. - Main Menu
    b. - Mobile Menu
		c. - Footer Menu

Gravity Forms
		a. - Change input to button allows for css psuedo class to use font icons

Woocommerce
    a. - Change number of products per page (12)
    b. - Create category dropdown menu

Videos
		a. - Youtube url convert to embed code

User Control
		a. - Remove Wordpress toolbar
		b. - Block wp-admin page (except for admin)

Tracking Codes
		a. - Googel Analytics
		b. - Facebook Pixel

Custom fields
		a. Social links

*/

/*********************
LAUNCH Core Functions
*********************/

/**
* Front end user profile form
*/

function my_acf_user_form_func( $atts ){

  $a = shortcode_atts( array(
    'field_group' => ''
  ), $atts );

  $uid = get_current_user_id();

  if ( ! empty ( $a['field_group'] ) && ! empty ( $uid ) ) {
    $options = array(
      'post_id' => 'user_'.$uid,
      'field_groups' => array( intval( $a['field_group'] ) ),
      'return' => add_query_arg( 'updated', 'true', get_permalink() )
    );

    ob_start();

    acf_form( $options );
    $form = ob_get_contents();

    ob_end_clean();
  }
    return $form;
}
add_shortcode('my_acf_user_form', 'my_acf_user_form_func');

function add_acf_form_head(){
  global $post;
  if (!empty($post) && has_shortcode( $post->post_content, 'my_acf_user_form' ) ) {
    acf_form_head();
  }
}
add_action( 'wp_head', 'add_acf_form_head', 7 );

// we're firing all out initial functions at the start
add_action( 'after_setup_theme', 'riz3_up', 16 );

function riz3_up() {

	// launching operation cleanup
	add_action( 'init', 'riz3_head_cleanup' );
	// remove WP version from RSS
	add_filter( 'the_generator', 'bones_rss_version' );
	// remove pesky injected css for recent comments widget
	add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
	// clean up comment styles in the head
	add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
	// clean up gallery output in wp
	add_filter( 'gallery_style', 'bones_gallery_style' );

	// enqueue base scripts and styles
	add_action( 'wp_enqueue_scripts', 'riz3_scripts_and_styles', 999 );

	// launch custom theme setup
	riz3_theme_support();

	// adding sidebars to Wordpress (these are created in core-functions.php)
	add_action( 'widgets_init', 'riz3_register_sidebars' );

	// cleaning up random code around images
	add_filter( 'the_content', 'bones_filter_ptags_on_images' );
	// cleaning up excerpt
	add_filter( 'excerpt_more', 'riz3_excerpt_more' );

} /* end riz3 up */

/*********************
WP_HEAD GOODNESS
The default wordpress head is
a mess. Let's clean it up by
removing all the junk we don't
need.
*********************/

function riz3_head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// index link
	remove_action( 'wp_head', 'index_rel_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// remove WP version from css
	add_filter( 'style_loader_src', 'bones_remove_wp_ver_css_js', 9999 );
	// remove Wp version from scripts
	add_filter( 'script_loader_src', 'bones_remove_wp_ver_css_js', 9999 );

} /* end riz3 head cleanup */

// remove WP version from RSS
function bones_rss_version() { return ''; }

// remove WP version from scripts
function bones_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}

// remove injected CSS for recent comments widget
function bones_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

// remove injected CSS from recent comments widget
function bones_remove_recent_comments_style() {
	global $wp_widget_factory;
	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
	}
}

// remove injected CSS from gallery
function bones_gallery_style($css) {
	return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}


/*********************
SCRIPTS & ENQUEUEING
*********************/

// loading modernizr and jquery, and reply script
function riz3_scripts_and_styles() {
	global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
	if (!is_admin()) {

		// Register main stylesheet
		wp_register_style( 'riz3-stylesheet', get_template_directory_uri() . '/dist/css/style.css', array(), '', 'all' );
		// Fonts
		wp_register_style( 'riz3-fonts', get_template_directory_uri() . '/lib/fonts/eurostile_cond/stylesheet.css', array(), '', 'all');

		// comment reply script for threaded comments
		if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
			wp_enqueue_script( 'comment-reply' );
		}


		//Remove default Wordpress Jquery version & load V3
		//wp_deregister_script( 'jquery' );
	 	//wp_enqueue_script( 'jquery-v3', get_template_directory_uri() .'/lib/js/min/jquery-3.2.1.min.js', array(), '3.2.1' );

	 // modernizr (without media query polyfill)
		wp_register_script( 'boxlayout-modernizr', get_template_directory_uri() . '/lib/js/modernizr.custom.js', array(), false );
		//adding custom scripts file in the footer
		wp_register_script( 'riz3-js', get_template_directory_uri() . '/dist/js/scripts.js', array( 'jquery' ), '', true );
		//Slick Slider
		wp_register_script( 'slick', get_template_directory_uri() . '/lib/js/min/slick.min.js', array(), '', false );


		// enqueue styles and scripts
		wp_enqueue_style( 'riz3-stylesheet' );
		//wp_enqueue_style( 'riz3-fonts' );
		wp_enqueue_style( 'riz3-ie-only' );

		$wp_styles->add_data( 'riz3-ie-only', 'conditional', 'lt IE 9' ); // add conditional wrapper around ie stylesheet

		//wp_enqueue_script( 'jquery-v3' );
		wp_enqueue_script( 'boxlayout-modernizr' );
		wp_enqueue_script( 'riz3-js' );
		wp_enqueue_script( 'slick' );
	}
}


/*******************************************************************************
-                               User Functions                                 -
*******************************************************************************/

/*
* Start - Remove Wordpress toolbar
*/
add_filter('show_admin_bar', '__return_false');

/*
* Start - Redirect non admin users to homepage from wp-admin
* This can be overridden in the child theme using 'riz3_block_wp_admin'
*/
if (!function_exists('riz3_block_wp_admin')){
  function riz3_block_wp_admin() {
  	if ( is_admin() && ! current_user_can( 'administrator' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
  		wp_safe_redirect( home_url() );
  		exit;
  	}
  }
  add_action( 'admin_init', 'riz3_block_wp_admin' );
}

/**
 * Start - Redirect users after logout to homepage
 */
function riz3_logout_redirect( $url ) {
    return home_url();
}
add_filter( 'logout_redirect', 'riz3_logout_redirect' );

/*
* Start - Custom Excerpt Length
*/
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

//Add current-menu-item class to custom post type (important for styling)
add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2 );
function add_current_nav_class($classes, $item) {

	// Getting the current post details
	global $post;

	// Getting the post type of the current post
	$current_post_type = get_post_type_object(get_post_type($post->ID));
	$current_post_type_slug = $current_post_type->rewrite[slug];

	// Getting the URL of the menu item
	$menu_slug = strtolower(trim($item->url));

	// If the menu item URL contains the current post types slug add the current-menu-item class
	if (strpos($menu_slug,$current_post_type_slug) !== false) {
	   $classes[] = 'current-menu-item';
	}

	// Return the corrected set of classes to be added to the menu item
	return $classes;
}

/*
* Start - Add Functions & Theme Support
*/
function riz3_theme_support() {

	//Enable single product slider
	add_theme_support( 'woocommerce' );
	//add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support( 'post-thumbnails' );

	// default thumb size
	set_post_thumbnail_size(125, 125, true);

	if ( function_exists( 'add_image_size' ) ) {
		add_image_size( 'bio-thumb', 300, 300, true ); //(cropped)
	}

	// wp custom background (thx to @bransonwerner for update)
	add_theme_support( 'custom-background',
		array(
		'default-image' => '',  // background image default
		'default-color' => '', // background color default (dont add the #)
		'wp-head-callback' => '_custom_background_cb',
		'admin-head-callback' => '',
		'admin-preview-callback' => ''
		)
	);

	// to add header image support go here: http://themble.com/support/adding-header-background-image-support/

	// uncomment to add post format support...these template pages do not exist and will need to be added.
	/*add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	); */

	// wp menus
	add_theme_support( 'menus' );
	// register menus
	register_nav_menus(
		array(
			'main-menu' => __( 'The Main Menu', 'riz3theme' ),      // main menu in header
			'mobile-menu' => __( 'Mobile Menu', 'riz3theme' ),      // mobile flyout menu
			'footer-menu' => __( 'Footer Menu', 'riz3theme' )       // footer menu
		)
	);
} /* End riz3 theme support */


/*******************************************************************************
-                             Menus & Navigation                               -
*******************************************************************************/

/*
* Start - The main menu
*/
function riz3_main_menu() {
	wp_nav_menu(array(
		'container_class' => 'main-menu-container',     // custom div container class (wraps ul)
		'menu_id' => 'main-menu',                       // custom ul id
		'menu' => __( 'The Main Menu', 'riz3' ),        // wp dashboard menu name
		'menu_class' => 'clearfix',                     // adding custom nav class
		'theme_location' => 'main-menu',                // use this for theme integration
		'before' => '',                                 // before the menu
		'after' => '',                                  // after the menu
		'link_before' => '',                            // before each link
		'link_after' => '',                             // after each link
		'depth' => 0,                                   // limit the depth of the nav
		'fallback_cb' => 'riz3_fallback_menu'           // fallback function
	));
} /* end main menu */

// mobile menu
function riz3_mobile_menu() {
	wp_nav_menu(array(
		'container_class' => 'mobile-menu-container',   // class of container
		'menu' => __( 'Mobile Menu', 'riz3' ),          // nav name
		'menu_class' => 'clearfix',                     // adding custom nav class
		'theme_location' => 'mobile-menu',              // where it's located in the theme
		'before' => '',                                 // before the menu
		'after' => '',                                  // after the menu
		'link_before' => '',                            // before each link
		'link_after' => '',                             // after each link
		'depth' => 0,                                   // limit the depth of the nav
		'fallback_cb' => 'riz3_fallback_menu'           // fallback function
	));
} /* end mobile menu */

// footer menu
function riz3_footer_menu() {
	wp_nav_menu(array(
		'container_class' => 'footer-menu-container ',  // class of container
		'menu' => __( 'Footer Menu', 'riz3' ),          // nav name
		'menu_class' => 'clearfix',                     // adding custom nav class
		'theme_location' => 'footer-menu',              // where it's located in the theme
		'before' => '',                                 // before the menu
		'after' => '',                                  // after the menu
		'link_before' => '',                            // before each link
		'link_after' => '',                             // after each link
		'depth' => 0,                                   // limit the depth of the nav
		'fallback_cb' => 'riz3_fallback_menu'           // fallback function
	));
} /* end footer menu */

// this is the fallback menu
function riz3_fallback_menu() {
	wp_page_menu( array(
		'show_home' => true,
		'menu_class' => 'nav clearfix',                 // adding custom nav class
		'include'     => '',
		'exclude'     => '',
		'echo'        => true,
		'link_before' => '',                            // before each link
		'link_after' => ''                              // after each link
	) );
} /* end fallback menu */


/**
 * Get taxonomies terms links.
 *
 * @see get_object_taxonomies()
 */
function riz3_custom_taxonomies_terms_links() {
    // Get post by post ID.
    $post = get_post( $post->ID );

    // Get post type by post.
    $post_type = $post->post_type;

    // Get post type taxonomies.
    $taxonomies = get_object_taxonomies( $post_type, 'objects' );

    $out = array();

    foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){

        // Get the terms related to post.
        $terms = get_the_terms( $post->ID, $taxonomy_slug );

        if ( ! empty( $terms ) ) {
            $out[] = "<h4>Category:</h4>\n<ul>";
            foreach ( $terms as $term ) {
                $out[] = sprintf( '<li>%2$s</li>',
                    esc_url( get_term_link( $term->slug, $taxonomy_slug ) ),
                    esc_html( $term->name )
                );
            }
            $out[] = "\n</ul>\n";
        }
    }
    return implode( '', $out );
}


//Gravity forms change input to button allows for css psuedo class to use font icons
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );
function form_submit_button ( $button, $form ){
    $button = str_replace( "input", "button", $button );
    $button = str_replace( "/", "", $button );
    $button .= "{$form['button']['text']}</button>";
    return $button;
}


//Youtube url convert to embed code
function youtube_embed_url($url)
{
    $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
    $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

    if (preg_match($longUrlRegex, $url, $matches)) {
        $youtube_id = $matches[count($matches) - 1];
    }

    if (preg_match($shortUrlRegex, $url, $matches)) {
        $youtube_id = $matches[count($matches) - 1];
    }
    return 'https://www.youtube.com/embed/' . $youtube_id ;
}



/*********************
Woocommerce
*********************/

//Check if woocommerce is activated
if ( ! function_exists( 'is_woocommerce_activated' ) ) {
	function is_woocommerce_activated() {
		if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
	}

	//Change number of products per page on the woocommerce shop
	add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );
	function new_loop_shop_per_page( $cols ) {
	  // $cols contains the current number of products per page based on the value stored on Options -> Reading
	  // Return the number of products you wanna show per page.
	  $cols = 12;
	  return $cols;
	}

	//Remove annoying InfusedWoo notification to update
	remove_action('admin_notices', 'iwpro_update_notices');

	//Category dropdown menu
	function riz3_taxonomy_dropdown($args) {
		$defaults = array(
			'hide_empty'        => 1,
		);

		$merged = wp_parse_args( $args, $defaults );
		$terms = get_terms( $merged );

		//$terms = wp_parse_args( $args, $default );
		?>
		<div class="select-wrap">
			<select id="cat">
				<option value="-1">Select Category</option>
				<?php foreach($terms as $cat)
				{
				echo '<option name="'.$cat->term_id.'" value="'.$cat->slug.'">'.$cat->name.'</option>';
				} ?>
				<option value="0">View All</option>
			</select>

			<script type='text/javascript'>
			/* <![CDATA[ */
				 var dropdown = document.getElementById("cat");
				 function onCatChange() {
					 if ( dropdown.options[dropdown.selectedIndex].value != -1 ) {
						 location.href = "./?product_cat="+dropdown.options[dropdown.selectedIndex].value;
					 }
					 if ( dropdown.options[dropdown.selectedIndex].value == 0){
						 location.href = "<?php echo esc_url( home_url( '/' ) ); ?>/products";
					 }
				 }
				 dropdown.onchange = onCatChange;
			/* ]]> */
			</script>
		</div>
		<?php

	   return ob_get_clean();
	}//End riz3_taxonomy_dropdown
}//End check if woocommerce is activated



/*********************
RELATED POSTS FUNCTION
*********************/

// Related Posts Function (call using bones_related_posts(); )
function bones_related_posts() {
	echo '<ul id="bones-related-posts">';
	global $post;
	$tags = wp_get_post_tags( $post->ID );
	if($tags) {
		foreach( $tags as $tag ) {
			$tag_arr .= $tag->slug . ',';
		}
		$args = array(
			'tag' => $tag_arr,
			'numberposts' => 5, /* you can change this to show more */
			'post__not_in' => array($post->ID)
		);
		$related_posts = get_posts( $args );
		if($related_posts) {
			foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
				<li class="related_post"><a class="entry-unrelated" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
			<?php endforeach; }
		else { ?>
			<?php echo '<li class="no_related_post">' . __( 'No Related Posts Yet!', 'riz3theme' ) . '</li>'; ?>
		<?php }
	}
	wp_reset_query();
	echo '</ul>';
} /* end bones related posts function */



/***********************************
Create theme options page with cmb2
************************************/

/**
 * This snippet has been updated to reflect the official supporting of options pages by CMB2
 * in version 2.2.5.
 */
add_action( 'cmb2_admin_init', 'riz3_register_theme_options_metabox' );
/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function riz3_register_theme_options_metabox() {
	/**
	 * Registers options page menu item and form.
	 */
	$cmb_options = new_cmb2_box( array(
		'id'           => 'riz3_option_metabox',
		'title'        => esc_html__( 'Site Options', 'riz3theme' ),
		'object_types' => array( 'options-page' ),
		/*
		 * The following parameters are specific to the options-page box
		 * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
		 */
		'option_key'      => 'riz3_options', // The option key and admin menu page slug.
		// 'icon_url'        => 'dashicons-palmtree', // Menu icon. Only applicable if 'parent_slug' is left empty.
		// 'menu_title'      => esc_html__( 'Options', 'myprefix' ), // Falls back to 'title' (above).
		// 'parent_slug'     => 'themes.php', // Make options page a submenu item of the themes menu.
		// 'capability'      => 'manage_options', // Cap required to view options-page.
		// 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
		// 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
		// 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
		// 'save_button'     => esc_html__( 'Save Theme Options', 'myprefix' ), // The text for the options-page save button. Defaults to 'Save'.
	) );
	/*
	 * Options fields ids only need
	 * to be unique within this box.
	 * Prefix is not needed.
	 */
	$cmb_options->add_field( array(
		'name' => __( 'Google Analytics', 'riz3theme' ),
		'desc' => __( 'Insert your Google GA Property ID', 'riz3theme' ),
		'id'   => 'analytics_id',
		'type' => 'text',
		'default' => '',
	) );
	$cmb_options->add_field( array(
		'name' => __( 'Facebook Pixel', 'riz3theme' ),
		'desc' => __( 'Insert your Facebook Pixel ID', 'riz3theme' ),
		'id'   => 'pixel_id',
		'type' => 'text',
	) );
	$cmb_options->add_field( array(
	'name' => 'Social Media Links',
	'desc' => 'Enter your social media account links below',
	'type' => 'title',
	'id'   => 'social_links'
	) );
	$cmb_options->add_field( array(
		'name' => __( 'Facebook Link', 'riz3theme' ),
		'desc' => __( 'Paste the link to your Facebook account here.', 'riz3theme' ),
		'id'   => 'fb_link',
		'type' => 'text_url',
		'protocols' => array( 'http', 'https'), // Array of allowed protocols
	) );
  $cmb_options->add_field( array(
    'name' => __( 'Instagram Link', 'riz3theme' ),
    'desc' => __( 'Paste the link to your Instagram account here.', 'riz3theme' ),
    'id'   => 'instagram_link',
    'type' => 'text_url',
    'protocols' => array( 'http', 'https'), // Array of allowed protocols
  ) );
	$cmb_options->add_field( array(
		'name' => __( 'Twitter Link', 'riz3theme' ),
		'desc' => __( 'Paste the link to your Twitter account here.', 'riz3theme' ),
		'id'   => 'twitter_link',
		'type' => 'text_url',
		'protocols' => array( 'http', 'https'), // Array of allowed protocols
	) );
  $cmb_options->add_field( array(
    'name' => __( 'Pinterest Link', 'riz3theme' ),
    'desc' => __( 'Paste the link to your Pinterest account here.', 'riz3theme' ),
    'id'   => 'pinterest_link',
    'type' => 'text_url',
    'protocols' => array( 'http', 'https'), // Array of allowed protocols
  ) );
	$cmb_options->add_field( array(
		'name' => __( 'Linkedin Link', 'riz3theme' ),
		'desc' => __( 'Paste the link to your Linkedin account here.', 'riz3theme' ),
		'id'   => 'linkedin_link',
		'type' => 'text_url',
		'protocols' => array( 'http', 'https'), // Array of allowed protocols
	) );
	$cmb_options->add_field( array(
		'name' => __( 'Youtube Link', 'riz3theme' ),
		'desc' => __( 'Paste the link to your Youtube account here.', 'riz3theme' ),
		'id'   => 'youtube_link',
		'type' => 'text_url',
		'protocols' => array( 'http', 'https'), // Array of allowed protocols
	) );
}
/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string $key     Options array key
 * @param  mixed  $default Optional default value
 * @return mixed           Option value
 */
function riz3_get_option( $key = '', $default = false ) {
	if ( function_exists( 'cmb2_get_option' ) ) {
		// Use cmb2_get_option as it passes through some key filters.
		return cmb2_get_option( 'riz3_options', $key, $default );
	}
	// Fallback to get_option if CMB2 is not loaded yet.
	$opts = get_option( 'riz3_options', $default );
	$val = $default;
	if ( 'all' == $key ) {
		$val = $opts;
	} elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
		$val = $opts[ $key ];
	}
	return $val;
}

//Tracking codes
function tracking_codes(){

	//Google Analytics integration
	$propertyID = riz3_get_option( 'analytics_id' );; // GA Property ID

	if (!$propertyID) { }else{?>
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', '<?php echo $propertyID; ?>']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
	<?php }
	//End Google Analytics integration


	//Facebook Pixel integration
	$pixelID = riz3_get_option( 'pixel_id' );
	if (!$pixelID) { }else{ ?>

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '<?php echo $pixelID; ?>'); // Insert your pixel ID here.
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=<?php echo $pixelID ?>&ev=PageView&noscript=1"
		/></noscript>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->
	<?php }

}//End tracking codes

// include tracking codes before the closing head tag
add_action('wp_head', 'tracking_codes');


/*********************
RANDOM CLEANUP ITEMS
*********************/

// remove the p from around imgs
function bones_filter_ptags_on_images($content){
	return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// Edit the excerpt link
function riz3_excerpt_more($more) {
	global $post;
	return '...  <a class="excerpt-read-more" href="'. get_permalink($post->ID) . '" title="'. __( 'Read', 'riz3theme' ) . get_the_title($post->ID).'">'. __( 'Read more', 'riz3theme' ) .'</a>';
}



?>
