<?php
/**
 * This is the default template
 *
 */
get_header(); ?>

<div id="page--<?php echo $post->ID; ?>" class="page page--default">

  <?php if (have_posts()) : while (have_posts()) : the_post();
    //Get the post content
    get_template_part( 'template-parts/content', 'page' );

  	if ( comments_open() || get_comments_number() ) :
  	    comments_template();
  	endif;

  //If no results exist
  endwhile; else :
    get_template_part( 'template-parts/content', 'none' );
  endif;
  ?>

</div><!-- .page -->

<?php get_footer(); ?>
