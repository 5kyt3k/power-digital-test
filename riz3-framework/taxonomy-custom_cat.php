<?php
/*
This is the custom post type taxonomy template.
If you edit the custom taxonomy name, you've got
to change the name of this template to
reflect that name change.

i.e. if your custom taxonomy is called
register_taxonomy( 'shoes',
then your single template should be
taxonomy-shoes.php

*/
?>

<?php get_header(); ?>

<div id="page--<?php echo $post->ID; ?>" class="page page--single">
	<div class="wrap">
		<div class="row">
			<div class="col-sm-8">
				<h1 class="archive-title h2"><span><?php _e( 'Posts Categorized:', 'riz3theme' ); ?></span> <?php single_cat_title(); ?></h1>

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article">
						<header class="article-header">
							<h3 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
							<p class="byline vcard"><?php
								printf( __( 'Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span> <span class="amp">&</span> filed under %4$s.', 'riz3theme' ), get_the_time( 'Y-m-j' ), get_the_time( __( 'F jS, Y', 'riz3theme' )), bones_get_the_author_posts_link(), get_the_term_list( get_the_ID(), 'custom_cat', "" ) );
							?></p>
						</header>

						<section class="entry-content">
							<?php the_excerpt( '<span class="read-more">' . __( 'Read More &raquo;', 'riz3theme' ) . '</span>' ); ?>
						</section>

						<footer class="article-footer">
						</footer>
					</article>

				<?php endwhile; ?>

						<?php if ( function_exists( 'bones_page_navi' ) ) { ?>
								<?php bones_page_navi(); ?>
						<?php } else { ?>
								<nav class="wp-prev-next">
										<ul class="clearfix">
											<li class="prev-link"><?php next_posts_link( __( '&laquo; Older Entries', 'riz3theme' )) ?></li>
											<li class="next-link"><?php previous_posts_link( __( 'Newer Entries &raquo;', 'riz3theme' )) ?></li>
										</ul>
								</nav>
						<?php } ?>

					<?php else :
			      get_template_part( 'template-parts/content', 'none' );
			    endif; ?>
			</div><!-- .col-sm-8 -->

			<div class="col-sm-4">
				<?php get_sidebar(); ?>
			</div><!-- .col-sm-4 -->
		</div><!-- .row -->
	</div><!-- .wrap -->
</div><!-- .page -->

<?php get_footer(); ?>
