<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
*/

get_header(); ?>

<div id="page--<?php echo $post->ID; ?>" class="page page--home">
  <?php
  if (have_posts()) : while (have_posts()) : the_post();
    //Get the post content
    get_template_part( 'template-parts/content', 'home' );
  endwhile; else :
    get_template_part( 'template-parts/content', 'none' );
  endif;
  ?>
</div><!-- .page -->

<?php get_footer(); ?>
