/*
Scripts File

Developed by:
URL:

Include custom js functions here. This file loads in the footer to help with load time.
*/



// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
  window.getComputedStyle = function(el, pseudo) {
    this.el = el;
    this.getPropertyValue = function(prop) {
      var re = /(-([a-z]){1})/g;
      if (prop == 'float') prop = 'styleFloat';
      if (re.test(prop)) {
        prop = prop.replace(re, function() {
          return arguments[2].toUpperCase();
        });
      }
      return el.currentStyle[prop] ? el.currentStyle[prop] : null;
    };
    return this;
  };
}


//As the page loads, call these scripts
jQuery(document).ready(function($) {
  //Needed for responsive youtube iframe embed
  $('.entry-content')
    .find('iframe[src*="youtube"]')
    .wrap("<div class='video-wrapper'/>");

  //Slick Slider
  $('.slider').slick({
    arrows: true,
    autoplay: true,
    autoplaySpeed: 9000
  });
  $('.stat-slider').slick({
    dots: false,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 4500
  });

  /*
  * Start - Fly out menu animations
  */
  $('.hamburger-icon').click(function(event) {
    event.preventDefault();
    $('#nav-icon').toggleClass('open');
    $('.flyout-menu--left').toggleClass('hide-left');
    $('.flyout-menu--right').addClass('hide-right');
    $('#account-close-menu').addClass('hide');
    $('#account-open-menu').removeClass('hide');
    return false;
  });

  $('.account-icon').click(function(event) {
    event.preventDefault();
    $('.flyout-menu--right').toggleClass('hide-right');
    $('.flyout-menu--left').addClass('hide-left');
    $('#nav-icon').removeClass('open');
    $('#account-close-menu').toggleClass('hide');
    $('#account-open-menu').toggleClass('hide');
    return false;
  });

  /*
  * Start - Video hover effect
  */
  $('.open-video').hover(
    function() {
      $(this)
        .children('video')
        .get(0)
        .play();
    },
    function() {
      $(this)
        .children('video')
        .get(0)
        .pause();
      $(this)
        .children('video')
        .get(0).currentTime;
    }
  );
  //End video hover effect


  //Player Pie Chart animation


  //Start popup
  $('.open-video').click(function() {
    //$(this).siblings().find("iframe")[0].src += "&autoplay=1";
    $(this)
      .parent()
      .find('.popup-video')
      .addClass('flex');
  });

  $('.close-btn').click(function() {
    $(this)
      .parent()
      .removeClass('flex');
    var video = $(this)
      .siblings()
      .find('iframe')
      .attr('src');
    $(this)
      .siblings()
      .find('iframe')
      .attr('src', '');
    $(this)
      .siblings()
      .find('iframe')
      .attr('src', video);
  });
  //End popup

  $('.close').click(function() {
    $(this)
      .parent()
      .removeClass('flex');
  });
  //End close

  /* getting viewport width */
  var responsive_viewport = $(window).width();

  /* if is below 768px */
  if (responsive_viewport < 768) {
  } /* end 768px screen */

  /* if is above or equal to 768px */
  if (responsive_viewport >= 768) {
    /* load gravatars */
    $('.comment img[data-gravatar]').each(function() {
      $(this).attr('src', $(this).attr('data-gravatar'));
    });
  }

  /* Resize actions */
  $(window).resize(function() {
    if ($(window).width() < 768) {
    } else {
    }
  });

  /*Scripts to run after page elements are loaded (images, iframes, etc..) */
  $(window).load(function() {
    setTimeout(function() {
      $('.popup-opt-in').addClass('flex');
    }, 5000);
  }); /* end of window load function */
}); /* end of as page load scripts */

/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
// (function(w) {
//   // This fix addresses an iOS bug, so return early if the UA claims it's something else.
//   //if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
//   var doc = w.document;
//   if (!doc.querySelector) {
//     return;
//   }
//   var meta = doc.querySelector('meta[name=viewport]'),
//     initialContent = meta && meta.getAttribute('content'),
//     disabledZoom = initialContent + ',maximum-scale=1',
//     enabledZoom = initialContent + ',maximum-scale=10',
//     enabled = true,
//     x,
//     y,
//     z,
//     aig;
//   if (!meta) {
//     return;
//   }
//   function restoreZoom() {
//     meta.setAttribute('content', enabledZoom);
//     enabled = true;
//   }
//   function disableZoom() {
//     meta.setAttribute('content', disabledZoom);
//     enabled = false;
//   }
//   function checkTilt(e) {
//     aig = e.accelerationIncludingGravity;
//     x = Math.abs(aig.x);
//     y = Math.abs(aig.y);
//     z = Math.abs(aig.z);
//     // If portrait orientation and in one of the danger zones
//     if (
//       !w.orientation &&
//       (x > 7 || (((z > 6 && y < 8) || (z < 8 && y > 6)) && x > 5))
//     ) {
//       if (enabled) {
//         disableZoom();
//       }
//     } else if (!enabled) {
//       restoreZoom();
//     }
//   }
//   w.addEventListener('orientationchange', restoreZoom, false);
// })(this);
