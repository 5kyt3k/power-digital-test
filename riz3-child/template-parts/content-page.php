<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package
 */
?>

<article id="post--<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry--header">
		<div class="wrap">
			<h1 class="entry--title"><?php the_title(); ?></h1>
		</div><!-- .wrap -->
	</header>

	<section class="entry--content">
    <div class="wrap-sm">
      <?php the_content(); ?>
    </div><!-- .wrap-sm -->
	</section>

	<footer class="entry--footer">
	</footer>
</article>
