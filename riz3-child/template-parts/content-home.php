<?php
/**
 * Template part for home page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package
 */
?>

<?php
$image = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'full');
?>

<div id="home-hero" class="banner" style="background-image: url(<?php echo $image ?>)">
  <h1 class="hero--title">Adventure</h1>
  <div class="home-hero--asset">
    <img src="<?php the_field('banner_asset'); ?>" />
  </div><!-- asset -->
</div><!-- .banner -->

<div id="people">
  <h2>People</h2>

  <div class="wrap">
    <h4>Travelers</h4>
    <div class="row">
      <?php
      $posts = get_posts(array(
      	'posts_per_page'	=> -1,
      	'post_type'			=> 'people'
      ));

      if( $posts ): ?>

      	<?php foreach( $posts as $post ):
      		setup_postdata( $post ); ?>
          <div class="col-xs-6 col-md-3">
        		<div class="person">

              <div class="person--thumbnail">
                <?php the_post_thumbnail('full');?>
              </div><!-- thumbnail -->

              <div class="person--title">
                <?php the_title(); ?>
              </div><!-- title -->

        			<a class="person--link" href="<?php the_permalink(); ?>">View Bio</a>
        		</div><!-- .person -->
          </div><!-- .col-md-3 -->
      	<?php endforeach; ?>

      	</ul>
      	<?php wp_reset_postdata(); ?>
      <?php endif; ?>
    </div><!-- .row -->
  </div><!-- wrap -->
</div><!-- people -->


<div id="places">
  <h2>Places</h2>
  <div class="wrap">
    <?php
    $i = 0;
    $posts = get_posts(array(
    	'posts_per_page'	=> -1,
    	'post_type'			=> 'places'
    ));

    if( $posts ): ?>

    	<?php foreach( $posts as $post ):
    		setup_postdata( $post );
        $i++;
        if ($i % 2 == 0){
          echo '<div class="place flip">';
        } else {
          echo '<div class="place">';
        } ?>

          <div class="place--content">
            <h3><?php the_title(); ?></h3>
            <?php the_content(); ?>
          </div><!-- title -->

          <div class="place--thumbnail">
            <?php the_post_thumbnail('full');?>
          </div><!-- thumbnail -->

    		</div><!-- place -->
    	<?php endforeach; ?>

    	</ul>
    	<?php wp_reset_postdata(); ?>
    <?php endif; ?>
  </div><!-- wrap -->
</div><!-- places -->

<div id="gallery">
  <h2>Gallery</h2>
  <div class="wrap">
    <div class="row">
      <div class="col-sm-4">
      </div><!-- .col-sm-4 -->
      <div class="col-sm-4">
      </div><!-- .col-sm-4 -->
      <div class="col-sm-4">
      </div><!-- .col-sm-4 -->
    </div><!-- .row -->
  </div><!-- .row -->
</div><!-- gallery -->
