<?php
/*
This file handles the admin area and functions.
You can use this file to make changes to the
dashboard.

Developed by:
URL:

*/

/************* CUSTOM LOGIN PAGE *****************/

// calling your own login css so you can style it
function riz3_child_login_css() {
	wp_enqueue_style( 'riz3_child_login_css', get_stylesheet_directory_uri() . '/lib/css/login.css', false );
}

// calling it only on the login page
add_action( 'login_enqueue_scripts', 'riz3_child_login_css', 10 );


/************* CUSTOMIZE ADMIN *******************/

/*
I don't really recommend editing the admin too much
as things may get funky if WordPress updates.
*/

?>
