<?php
/*
This is the custom post type post template.
If you edit the post type name, you've got
to change the name of this template to
reflect that name change.

i.e. if your custom post type is called
register_post_type( 'bookmarks',
then your single template should be
single-bookmarks.php

*/
?>

<?php get_header(); ?>

<div class="page">
	<div class="wrap">
		<div class="col-sm-8">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
					<header class="article-header">

						<h1 class="single-title custom-post-type-title"><?php the_title(); ?></h1>
						<p class="byline vcard"><?php
							printf( __( 'Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span> <span class="amp">&</span> filed under %4$s.', 'riz3theme' ), get_the_time( 'Y-m-j' ), get_the_time( __( 'F jS, Y', 'riz3theme' ) ), bones_get_the_author_posts_link(), get_the_term_list( $post->ID, 'custom_cat', ' ', ', ', '' ) );
						?></p>

					</header>
					<section class="entry-content clearfix">

						<?php the_content(); ?>

					</section>
					<footer class="article-footer">
						<p class="tags"><?php echo get_the_term_list( get_the_ID(), 'custom_tag', '<span class="tags-title">' . __( 'Custom Tags:', 'riz3theme' ) . '</span> ', ', ' ) ?></p>
					</footer>

					<?php comments_template(); ?>
				</article>

			<?php endwhile; ?>
			<?php else :
				get_template_part( 'template-parts/content', 'none' );
			endif; ?>
		</div><!-- .col-sm-8 -->

		<div class=".col-sm-4">
			<?php get_sidebar(); ?>
		</div><!-- .col-sm-4 -->
	</div><!-- .wrap -->
</div><!-- .page -->

<?php get_footer(); ?>
