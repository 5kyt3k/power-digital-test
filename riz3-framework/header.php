<!doctype html>
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<?php // Google Chrome Frame for IE ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php wp_title(''); ?></title>

	<link rel="icon" type="image/png" sizes="192x192" href="<?php echo get_template_directory_uri(); ?>/lib/favicons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/lib/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/lib/favicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/lib/favicons/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<script src="https://d3js.org/d3.v5.min.js"></script>

	<?php // wordpress head functions ?>
	<?php wp_head(); ?>
	<?php // end of wordpress head ?>
</head>

<body <?php body_class(); ?>>
	<header class="header">
		<div class="wrap">

			<div class="row">
				<div class="col-xs header--left">
					<a class="logo" href="<?php echo home_url();?>">
						<img src="<?php echo get_template_directory_uri();?>/lib/images/logo.svg" />
					</a><!-- .logo -->
				</div><!-- .col-xs-->
				<div class="col-xs end-xs header--right">

					<?php riz3_main_menu(array( 'theme_location' => 'main-menu' )); ?>

					<div class="menu-btn-container">
						<button id="nav-icon">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</button>
					</div><!-- .menu-btn-container -->

					<div class="flyout-menu-container hide">
						<div class="wrap">
							<?php riz3_mobile_menu(array( 'theme_location' => 'mobile-menu' )); ?>
						</div><!-- .wrap -->
					</div><!-- .flyout-menu-container -->

				</div><!-- .col-xs-->
			</div><!-- .row-->
		</div><!-- .wrap-full -->
	</header>
