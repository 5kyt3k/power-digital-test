<?php
/*
Template Name: Sample Template
*/

get_header(); ?>

<div id="page--<?php echo $post->ID; ?>" class="page page--home">
  <div class="wrap">
    <div class="row">
      <div class="col-sm-8">
        <?php
          if (have_posts()) : while (have_posts()) : the_post();
          //Get the post content
          get_template_part( 'template-parts/content', 'page' );
        endwhile; else :
          get_template_part( 'template-parts/content', 'none' );
        endif;
        ?>
      <div class="col-sm-4">
        <?php get_sidebar(); ?>
      </div><!-- .col-sm-4 -->
    </div><!-- .row -->
  </div><!-- .wrap -->
</div><!-- .page -->

<?php get_footer(); ?>
