<?php
/**
 * The template for 404 error
 *
 * @package
 * @subpackage
 * @since
 */

get_header(); ?>

<div id="page--<?php echo $post->ID; ?>" class="page page--404">
  <div class="wrap-sm">
    <div class="col-sm-8">
      <article id="post-not-found" class="hentry clearfix">
        <header class="article-header">
          <h1><?php _e( '404 - Not Found', 'riz3theme' ); ?></h1>
        </header>
        <section class="entry-content">
          <p><?php _e( 'I am sorry but what you were looking for was not found, but maybe try looking again!', 'riz3theme' ); ?></p>
        </section>

        <section class="search">
          <p><?php get_search_form(); ?></p>
        </section>

        <footer class="article-footer">
          <p><?php _e( 'If you have any questions please contact us.', 'riz3theme' ); ?></p>
        </footer>
      </article>
    </div><!-- .col-sm-8 -->
  </div><!-- .wrap -->
</div><!-- .page -->

<?php get_footer(); ?>
