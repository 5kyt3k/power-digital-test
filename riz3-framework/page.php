<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package
 * @subpackage
 * @since
 */

get_header(); ?>

<div id="page--<?php echo $post->ID; ?>" class="page page--home">
  <?php
    if (have_posts()) : while (have_posts()) : the_post();
    //Get the post content
    get_template_part( 'template-parts/content', 'page' );
  endwhile; else :
    get_template_part( 'template-parts/content', 'none' );
  endif;
  ?>
</div><!-- .page -->

<?php get_footer(); ?>
