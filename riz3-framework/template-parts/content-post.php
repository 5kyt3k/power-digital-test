<?php
/**
 * Template part for posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package
 */
?>

<a class="thumbnail-link" href="<?php the_permalink(); ?>">
	<?php
	if ( has_post_thumbnail() ) {
		the_post_thumbnail('large');
	} else { ?>
		<img src="<?php echo get_template_directory_uri(); ?>/lib/images/default-image.jpg" />
	<?php } ?>

	<span class="overlay"><button>Read More</button></span>
</a>
<div class="post-excerpt-container">
	<article>
		<header class="article--header">
			<div class="category-links">
				<?php the_category(', '); ?>
			</div><!-- .category-links -->
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		</header>

		<section class="entry-content">
			<?php the_excerpt(); ?>
		</section>

		<footer class="article--footer">
			<?php the_date(); ?>
			<div class="box box--share-on-social">
				<span class="share-title">Share</span>
				<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
					<span class="icons icon-facebook2"></span>
				</a>
				<a href="https://twitter.com/share?ref_src=twsrc%5Etfw" target="_blank" class="twitter-share-button" data-show-count="false"><i class="icons icon-twitter"></i></a>
				<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" target="_blank"><i class="icons icon-linkedin"></i></a>
			</div><!-- .box -->
		</footer>
	</article>
</div><!-- .post-excerpt-container -->
